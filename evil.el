

;; Evil
(use-package evil :ensure t
  :config (evil-mode 1))

(add-hook 'text-mode-hook 'evil-local-mode)
(add-hook 'prog-mode-hook 'evil-local-mode)




(defun copy-to-end-of-line ()
  (interactive)
  (evil-yank (point) (point-at-eol)))

(define-key evil-normal-state-map "Y" 'copy-to-end-of-line)

;; These get me all the time.
(evil-ex-define-cmd "E[dit]" 'evil-edit)
(evil-ex-define-cmd "W[rite]" 'evil-write)

;; Indent whole region sensibly
(defun evil-shift-right-and-indent()
  (interactive)
  (evil-indent (region-beginning) (region-end))
  (indent-region (region-beginning) (region-end)))

(define-key evil-visual-state-map ">" 'evil-shift-right-and-indent)

(defun newline-without-break-of-line ()
  (interactive)
  (let ((oldpos (point)))
    (end-of-line)
    (newline-and-indent)))
(define-key evil-normal-state-map "\\" 'newline-without-break-of-line)




(setq evil-default-state 'normal)
(define-key evil-normal-state-map (kbd ";") 'evil-ex)
(define-key evil-normal-state-map (kbd "SPC") 'evil-ex)
(define-key evil-normal-state-map "\C-y" 'yank)
(define-key evil-insert-state-map "\C-y" 'yank)
(define-key evil-normal-state-map "\C-w" 'evil-delete)
(define-key evil-insert-state-map "\C-w" 'evil-delete)
(define-key evil-insert-state-map "\C-a" 'evil-beginning-of-line)
(define-key evil-insert-state-map "\C-e"
  (lambda () (interactive)
    (evil-end-of-line)
    (forward-char)))



;;; Escape the minibuffer with escape
(define-key  evil-normal-state-map            [escape]  'keyboard-quit)
(define-key  evil-visual-state-map            [escape]  'keyboard-quit)
(define-key  minibuffer-local-map             [escape]  'keyboard-escape-quit)
(define-key  minibuffer-local-ns-map          [escape]  'keyboard-escape-quit)
(define-key  minibuffer-local-completion-map  [escape]  'keyboard-escape-quit)
(define-key  minibuffer-local-must-match-map  [escape]  'keyboard-escape-quit)
(define-key  minibuffer-local-isearch-map     [escape]  'keyboard-escape-quit)






;; Initialize in mode X
(mapc (lambda (pair)
        (let
            ((mode  (car pair))
             (state (cadr pair)))
          (evil-set-initial-state mode state)))
      '((inferior-emacs-lisp-mode emacs)
        (haskell-interactive-mode emacs)
        (inferior-haskell-mode    emacs)
        (neotree-mode             emacs)
        (inferior-ess-mode        emacs)
        (shell-mode               emacs)
        (jupyter-repl-mode        emacs)
        (inferior-ess-mode        emacs)
        (dired-mode               emacs)
        (circe-channel-mode       emacs)
        (wdired-mode              normal)))



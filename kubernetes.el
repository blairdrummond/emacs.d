(add-hook 'k8s-mode-hook 'yas-minor-mode)

(use-package k8s-mode
 :ensure t
 :config
 (setq k8s-search-documentation-browser-function 'browse-url-firefox)
 :hook (k8s-mode . yas-minor-mode))


(use-package docker :ensure t)
;(use-package lsp-docker :ensure t)
(use-package dockerfile-mode :ensure t)
(use-package docker-compose-mode :ensure t)
(use-package terraform-mode :ensure t)

(use-package kubernetes :ensure t)
(use-package kubernetes-tramp :ensure t)
(use-package kubernetes-helm :ensure t)
(use-package kubel :ensure t)

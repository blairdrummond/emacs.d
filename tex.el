;; TeX
(setq-default TeX-PDF-mode t)
(fset 'tex-font-lock-suscript 'ignore)
(add-hook 'LaTeX-mode-hook 'visual-line-mode)
(add-hook 'LaTeX-mode-hook
          (lambda()
	    (setq TeX-save-query nil)
	    (setq TeX-show-compilation nil)))

(add-hook 'plain-tex-mode-hook 'flyspell-mode)
(add-hook 'plain-tex-mode-hook
          (lambda()
	    (setq TeX-save-query nil)
	    (setq TeX-show-compilation nil)))

;; No super/sub-scripts
(setq tex-fontify-script nil)
(setq font-latex-fontify-script nil)

; (setq auto-mode-alist (cons '("\\.tex$" . plain-tex-mode) auto-mode-alist))




(use-package ivy       :ensure t)
(use-package which-key :ensure t)
(use-package smex      :ensure t)
(use-package counsel   :ensure t)



(which-key-mode)

(ivy-mode 1)
(setq ivy-use-virtual-buffers t)
(setq ivy-display-style nil)
(setq ivy-format-function (quote ivy-format-function-arrow))
(setq ivy-height 20)



;; counsel
(global-set-key (kbd "M-x") 'counsel-M-x)


;; swiper
(global-set-key "\C-s" 'swiper)
(define-key evil-normal-state-map (kbd "/") 'swiper)


;; ibuffer
(defalias 'list-buffers 'ivy-switch-buffer)

(setq org-journal-enable-agenda-integration t)

(setq org-agenda-sticky t)
;; (setq org-agenda-file-regexp "\\`[^.].*\\.org\\'\\|\\`[0-9]+\\'")
(setq show-week-agenda-p t)


(org-babel-do-load-languages
 'org-babel-load-languages
 '((haskell . t)
   (python . t)
   (C . t)))


 (setq org-todo-keywords
	   '((sequence
		  "TODO"
		  "|"
		  "DONE")))

(run-with-idle-timer 60 t 'org-save-all-org-buffers)
(setq org-agenda-start-day "-0d")
(setq org-agenda-span 10)
(setq org-agenda-start-on-weekday nil)


(use-package calfw :ensure t)
(use-package calfw-org :ensure t)
(require 'calfw-org)

;; use org agenda buffer style keybinding.
(setq cfw:org-overwrite-default-keybinding t)

(defun org-cal ()
  (interactive)
  (cfw:open-org-calendar))

(setq cfw:fchar-junction ?╋
      cfw:fchar-vertical-line ?┃
      cfw:fchar-horizontal-line ?━
      cfw:fchar-left-junction ?┣
      cfw:fchar-right-junction ?┫
      cfw:fchar-top-junction ?┯
      cfw:fchar-top-left-corner ?┏
      cfw:fchar-top-right-corner ?┓)
      












;; https://raw.githubusercontent.com/psamim/dotfiles/master/doom/config.el


;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq
  org-ellipsis "…"
   ;; ➡, ⚡, ▼, ↴, , ∞, ⬎, ⤷, ⤵, ❱
  org-deadline-warning-days 7
  org-agenda-breadcrumbs-separator " » "
  )


(setq
  org-log-done t
  org-image-actual-width '(700)
  org-clock-into-drawer t
  org-clock-persist t
  org-columns-default-format "%60ITEM(Task) %20TODO %10Effort(Effort){:} %10CLOCKSUM"
  org-global-properties (quote (("Effort_ALL" . "0:15 0:30 0:45 1:00 2:00 3:00 4:00 5:00 6:00 0:00")
                                ("STYLE_ALL" . "habit")))
  ;; org-plantuml-jar-path (expand-file-name "~/Downloads/plantuml.jar")
  ;; org-export-babel-evaluate nil
  org-confirm-babel-evaluate nil
  ;; org-todo-keywords '((sequence "TODO" "WAITING" "|" "DONE"))
  org-duration-format '((special . h:mm))
  org-time-clocksum-format (quote (:hours "%d" :require-hours t :minutes ":%02d" :require-minutes t))
  bidi-paragraph-direction t
  ;; org-caldav-url 'google
  ;; org-caldav-calendar-id "ovuticv96133cisuc0pm8f7d6g@group.calendar.google.com"
  ;; org-caldav-files '("~/Notes/appointments.org")
  ;; org-caldav-oauth2-client-id "279358326453-ar2bfnerndjnnie90e59i9otuif9ut84.apps.googleusercontent.com"
  ;; org-caldav-oauth2-client-secret "SECRET"
  ;; org-caldav-inbox "~/Notes/calendar-inbox.org"
  org-hide-emphasis-markers t
  ;; org-fontify-done-headline t
  ;; org-fontify-whole-heading-line t
  ;; org-fontify-quote-and-verse-blocks t
  )


; (customize-set-value
;     'org-agenda-category-icon-alist
;     `(
;       ("work" "~/.dotfiles/icons/money-bag.svg" nil nil :ascent center)
;       ("chore" "~/.dotfiles/icons/loop.svg" nil nil :ascent center)
;       ("events" "~/.dotfiles/icons/calendar.svg" nil nil :ascent center)
;       ("todo" "~/.dotfiles/icons/checklist.svg" nil nil :ascent center)
;       ("walk" "~/.dotfiles/icons/walk.svg" nil nil :ascent center)
;       ("solution" "~/.dotfiles/icons/solution.svg" nil nil :ascent center)
;       ))



;; (defun eh-org-agenda-change-breadcrumbs-color ()
;;   (save-excursion
;;     (goto-char (point-min))
;;     (while (re-search-forward org-agenda-breadcrumbs-separator nil t)
;;       (put-text-property (match-beginning 0) (match-end 0)
;;                          'face '(:foreground "grey" :bold t)))))

(setq org-agenda-block-separator (string-to-char " "))
(setq org-agenda-format-date 'my-org-agenda-format-date-aligned)

(defun my-org-agenda-format-date-aligned (date)
  "Format a DATE string for display in the daily/weekly agenda, or timeline.
This function makes sure that dates are aligned for easy reading."
  (require 'cal-iso)
  (let* ((dayname (calendar-day-name date 1 nil))
         (day (cadr date))
         (persian (substring (calendar-persian-date-string date) 0 -6))
         (day-of-week (calendar-day-of-week date))
         (month (car date))
         (monthname (calendar-month-name month 1))
         (year (nth 2 date))
         (iso-week (org-days-to-iso-week
                    (calendar-absolute-from-gregorian date)))
         (weekyear (cond ((and (= month 1) (>= iso-week 52))
                          (1- year))
                         ((and (= month 12) (<= iso-week 1))
                          (1+ year))
                         (t year)))
         (weekstring (if (= day-of-week 1)
                         (format " W%02d" iso-week)
                       "")))
         (format "%-2s. %2d %s, %s"
            dayname day monthname persian)))

(setq org-agenda-custom-commands
      '(("o" "My Agenda"
         ((todo "TODO" (
                      (org-agenda-overriding-header "\n⚡ Do Today:\n⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺")
                      (org-agenda-remove-tags t)
                      (org-agenda-prefix-format " %-2i %-15b")
                      (org-agenda-todo-keyword-format "")
                       ))
          (agenda "" (
                      (org-agenda-start-day "+0d")
                      (org-agenda-span 5)
                      (org-agenda-overriding-header "⚡ Schedule:\n⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺")
                      (org-agenda-repeating-timestamp-show-all nil)
                      (org-agenda-remove-tags t)
                      (org-agenda-prefix-format   "  %-3i  %-15b - %t%s")
                      (org-agenda-todo-keyword-format " !!! ")
                      (org-agenda-current-time-string "»»»»»»»» now")
                      (org-agenda-scheduled-leaders '("" ""))
                      (org-agenda-time-grid (quote ((daily today remove-match)
                                                    (0900 1200 1500 1800 2100)
                                                    "      " "┈┈┈┈┈┈┈┈┈┈┈┈┈")))
                       ))
          ))))



;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type nil)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.

;; (after! org-mode
;;   (set-company-backend! 'company-dabbrev)
;;   )
;;

;; (add-hook 'text-mode-hook
;;            (lambda ()
;;             (variable-pitch-mode 1)))

;; Transparency
;(set-frame-parameter (selected-frame) 'alpha '(92 92))
;(add-to-list 'default-frame-alist '(alpha . (92 . 92)))


;(add-hook 'org-agenda-finalize-hook #'set-window-clean)

(set-face-attribute 'org-agenda-date-weekend nil :weight 'semi-bold :height 1.2)
(set-face-attribute 'org-agenda-date-today   nil :weight 'semi-bold :height 1.2)
(set-face-attribute 'org-agenda-date         nil :weight 'semi-bold :height 1.2)
(set-face-attribute 'org-agenda-structure    nil :weight 'semi-bold :height 1.2)


(add-hook
 'org-agenda-finalize-hook
 (lambda () (setq org-agenda-files (directory-files (expand-file-name "/home/blair/org.d/") 1 "^\\([^.]\\|\\.[^.]\\|.projectile\\|\\.\\..\\)"))))

(setq org-agenda-files (directory-files (expand-file-name "/home/blair/org.d/") 1 "^\\([^.]\\|\\.[^.]\\|.projectile\\|\\.\\..\\)"))

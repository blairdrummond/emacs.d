;; Generic things for emacs behaviour


(setq gc-cons-threshold 200000000)
(setq browse-url-browser-function 'browse-url-chromium)
(setq debug-on-quit nil)
(setq make-backup-files nil)
(setq scroll-step 1) ;; keyboard scroll one line at a time
(setq vc-follow-symlinks t)

(fset 'yes-or-no-p 'y-or-n-p)

(setq
 compilation-always-kill t              ; Never prompt to kill a compilation session.
 compilation-scroll-output 'first-error ; Always scroll to the bottom.
 make-backup-files nil                  ; No backups, thanks.
 create-lockfiles nil                   ; Emacs sure loves to put lockfiles everywhere.
 kill-whole-line t                      ; Delete the whole line if C-k is hit at the beginning of a line.
 require-final-newline t                ; Auto-insert trailing newlines.
 ring-bell-function 'ignore             ; Do not ding. Ever.
 use-dialog-box nil                     ; Dialogues always go in the modeline.
 frame-title-format "emacs - %b"        ; Put something useful in the status bar.
 initial-scratch-message nil            ; SHUT UP SHUT UP SHUT UP
 save-interprogram-paste-before-kill t  ; preserve paste to system ring
 enable-recursive-minibuffers t         ; don't fucking freak out if I use the minibuffer twice
 sentence-end-double-space nil          ; are you fucking kidding me with this shit
 debug-on-quit nil
 )



(setq scroll-step            1
	  scroll-conservatively  10000)

(use-package smooth-scrolling :ensure t)
(require 'smooth-scrolling)
(smooth-scrolling-mode 1)





;; scroll one line at a time (less "jumpy" than defaults)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq mouse-wheel-progressive-speed nil)
(setq redisplay-dont-pause t)

(setq scroll-margin 100
      scroll-conservatively 0
      scroll-up-aggressively 0.01
      scroll-down-aggressively 0.01)
(setq-default scroll-up-aggressively 0.01
			  scroll-down-aggressively 0.01)

; Autosave every 500 typed characters
(setq auto-save-interval 500)



(when (get-buffer "*Messages*")
  (kill-buffer "*Messages*"))



;; default minibuffer is stubborn.
(defun close-the-damn-minibuffer ()
  (interactive)
  (ignore-errors
	(setq debug-on-error nil)
	(setq debug-on-quit nil)
	(progn
	  (with-selected-window
		  (minibuffer-window nil)
		(minibuffer-keyboard-quit)))))


(global-set-key [escape] 'close-the-damn-minibuffer)

(defun normal-state-and-close-buf ()
  (interactive)
  (close-the-damn-minibuffer)
  (evil-force-normal-state))





;; ibuffer
(use-package ibuffer :ensure t)
(require 'ibuffer)
(setq ibuffer-saved-filter-groups
      (quote (("default"
	       ("Pdf" (name . ".*\\.pdf"))
	       ("Tramp" (filename . "/ssh:.*"))
	       ("Organization" (or (name . "todo.*")
				   (name . ".*\\.todo")
				   (name . ".*\\.rem")
				   (name . ".*\\.org")))

	       ("Text" (or (name . ".*\\.txt")
			   (name . ".*\\.md")
			   (name . ".*\\.tex")))

	       ("Programming" 
		(or
		 (name . "config.*")
		 (name . "TODO.*")
		 (name . "README.*")
		 (mode . haskell-mode)
		 (mode . python-mode)
		 (mode . julia-mode)
		 (mode . emacs-lisp-mode)
		 (mode . c-mode)
		 (mode . sh-mode)
		 (mode . shell-script-mode)
		 (mode . ess-mode)))

	       ("Folder" (mode . dired-mode))
	       ("emacs" (or (filename . ".*\\.emacs")
			    (filename . ".*\\.github-files")
			    (filename . ".*\\.scratchpad")
			    (filename . ".*\\.temp")))
	       ("*running*" (or
			     (mode . shell-mode)
			     (name . "\\*R\\*")))
	       ("*buffer*" (name . "\\*.*\\*"))))))


(add-hook 'ibuffer-mode-hook
	  (lambda ()
	    (ibuffer-switch-to-saved-filter-groups "default")))

(defalias 'buffer-menu 'ibuffer)

(setq ibuffer-formats
      (quote
       ((mark modified read-only " "
	      (name 30 30 :left :elide)
	      " "
	      (mode 16 16 :left :elide)
	      " " filename-and-process)
	(mark " "
	      (name 16 -1)
	      " " filename))))


;; Openwith
(use-package openwith :ensure t)
(require 'openwith)
(setq openwith-associations '(("\\.pdf\\'"  "zathura" (file))
			      ("\\.png\\'"  "feh" (file))
			      ("\\.jpg\\'"  "feh" (file))
			      ("\\.jpeg\\'" "feh" (file))))
(openwith-mode t)



;; Neotree
(use-package neotree :ensure t)
(global-set-key [M-tab] 'neotree-toggle)
(use-package all-the-icons :ensure t)
(setq neo-theme (if (display-graphic-p) 'icons 'arrow))




;; Projectile
(use-package projectile
 :config
    (projectile-global-mode)
    (setq projectile-project-search-path '("~/desk"))
    (setq projectile-completion-system 'ivy)
    (setq projectile-enable-caching t)
 :ensure t)
; (global-set-key (kbd "C-c p .") 'helm-projectile-find-file-dwim)




; ;; nlinum
; (use-package nlinum :ensure t)
; (add-hook 'prog-mode-hook 'nlinum-mode)


;; Dired
(use-package dired-hide-dotfiles :ensure t)
(defun my-dired-mode-hook ()
  "My `dired' mode hook."
  ;; To hide dot-files by default
  (dired-hide-dotfiles-mode)

  ;; To toggle hiding
  (define-key dired-mode-map "." #'dired-hide-dotfiles-mode))

(add-hook 'dired-mode-hook #'my-dired-mode-hook)





;; Windmove
(use-package windmove)
;; move between windows
(global-set-key (kbd "M-<down>")  'windmove-down)
(global-set-key (kbd "M-<up>")    'windmove-up)
(global-set-key (kbd "M-<left>")  'windmove-left)
(global-set-key (kbd "M-<right>") 'windmove-right)


(global-set-key (kbd "M-j")  'windmove-down)
(global-set-key (kbd "M-k")  'windmove-up)
(global-set-key (kbd "M-h")  'windmove-left)
(global-set-key (kbd "M-l")  'windmove-right)





;; Kill stupid buffers


;; Remove annoying buffers
(setq-default message-log-max nil)
(when (get-buffer "*Messages*")
(kill-buffer "*Messages*"))
(when (get-buffer "*ESS*")
(kill-buffer "*ESS*"))
(add-hook 'compilation-finish-functions
	(lambda
	(buf strg)
	(kill-buffer buf)))



;; Shell
(use-package "flycheck" :ensure t)
(add-hook 'sh-mode-hook 'flycheck-mode)


;; Flycheck
(defun flycheck-display-error-messages-unless-error-buffer (errors)
  (unless (get-buffer-window flycheck-error-list-buffer)
    (flycheck-display-error-messages errors)))

(setq flycheck-display-errors-function #'flycheck-display-error-messages-unless-error-buffer)

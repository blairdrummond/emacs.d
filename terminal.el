;; Tmux? St?
(unless (display-graphic-p)
  (progn
	(define-key input-decode-map "\e[1;5C" [(control right)])
	(define-key input-decode-map "\e[1;5D" [(control left)])
	(define-key input-decode-map "\e[1;5A" [(control up)])
	(define-key input-decode-map "\e[1;5B" [(control down)])))

